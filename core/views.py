# coding=utf-8
from __future__ import unicode_literals
from django.shortcuts import render, redirect, resolve_url
from django.contrib.auth import login as auth_login
from django.http import HttpResponse, HttpResponseRedirect


from core.models import User, UserPart, UserPartInfo
from .forms import LoginForm
from .tasks import parse_part


def index(request):
    user = User.objects.get(id=request.user.id)
    user_parts = user.user_parts.all()
    context = {'user_parts': user_parts}
    return render(request, 'core/index.html', context)


def info(request, user_part_id):
    part = UserPart.objects.get(id=user_part_id)
    part_info = part.info.all()
    context = {'part_info': part_info}
    return render(request, 'core/info.html', context)

