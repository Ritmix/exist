# coding=utf-8

from django.db import models
from django.contrib.auth.models import User


class UserPart(models.Model):
    user = models.ForeignKey(User, related_name='user_parts', verbose_name='Пользователь')
    article = models.CharField(max_length=64, blank=True, verbose_name='Артикул')
    id_number = models.CharField(max_length=64, blank=True, verbose_name='Идентификационный номер')
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата добавления')
    is_active = models.BooleanField(default=True, verbose_name='Активно')

    class Meta:
        ordering = ['-is_active', '-created_at']
        verbose_name = 'деталь'
        verbose_name_plural = 'Детали'

    def __unicode__(self):
        return self.article + self.id_number


class UserPartInfo(models.Model):
    part = models.ForeignKey(UserPart, related_name='info')
    firm = models.CharField(max_length=64, verbose_name='Фирма')
    description = models.CharField(max_length=128, verbose_name='Описание')
    in_stock = models.CharField(max_length=64, verbose_name='В наличии')
    minimal_order = models.IntegerField(default=1, verbose_name='Минимильный заказ')
    expected_time = models.CharField(max_length=64, verbose_name='Ожидаемый срок')
    price = models.CharField(max_length=64, verbose_name='Цена')
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата и время')

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.firm + ' ' + self.description + unicode(self.created_at)

