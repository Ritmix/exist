# coding: utf-8
from django.conf.urls import patterns, url

from django.views.generic import RedirectView

urlpatterns = patterns('core.views',
                       url(r'^$', 'index', name='index'),
                       url(r'^(.*)/$', 'info', name='info'))
