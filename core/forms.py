# coding: utf-8

from __future__ import unicode_literals

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse_lazy

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Div, HTML, BaseInput, Button
from crispy_forms.bootstrap import FormActions

from core.models import User


class LoginForm(AuthenticationForm):
    error_messages = {
        'invalid': 'Неверный логин или пароль.',
        'invalid_login': 'Неверный логин или пароль.',
    }

    username = forms.CharField(label='', max_length=64, required=True)
    password = forms.CharField(label='', widget=forms.PasswordInput(render_value=False), required=True)

    def __init__(self, request=None, *args, **kwargs):
        super(LoginForm, self).__init__(request, *args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Логин'
        self.fields['password'].widget.attrs['placeholder'] = 'Пароль'
        self.helper = FormHelper()
        self.helper.form_action = reverse_lazy('login')
        self.helper.form_method = 'post'
        self.helper.form_class = 'form-vertical login-form'
        self.helper.layout = Layout(
            HTML('<h3 class="form-title">Вход</h3>'),
            Div(
                HTML('<i class="glyphicon glyphicon-user"></i>'),
                Field('username'),
                css_class='input-icon'
            ),
            Div(
                HTML('<i class="glyphicon glyphicon-lock"></i>'),
                Field('password'),
                css_class='input-icon'
            ),
            FormActions(
                HTML("""
                    <button type="submit" class="submit btn btn-primary pull-right">
                        Войти
                    </button>
                """)
            )
        )

