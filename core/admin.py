from django.contrib import admin

from core.models import UserPart, UserPartInfo


class UserPartAdmin(admin.ModelAdmin):
    list_display = ('article', 'created_at', 'is_active')


class UserPartInfoAdmin(admin.ModelAdmin):
    list_display = ('part', 'firm', 'description', 'in_stock', 'minimal_order', 'expected_time', 'price', 'created_at')


admin.site.register(UserPart, UserPartAdmin)
admin.site.register(UserPartInfo, UserPartInfoAdmin)
