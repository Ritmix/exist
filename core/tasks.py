# coding=utf-8
from __future__ import unicode_literals

from BeautifulSoup import BeautifulSoup
import urllib2
import datetime

from .models import UserPart, UserPartInfo


def update_user_part_info():
    for user_part_id in UserPart.objects.filter(is_active=True).values_list('id', flat=True):
        parse_part(user_part_id)


def parse_part(user_part_id):
    user_part = UserPart.objects.get(id=user_part_id)

    if user_part.article:
        url = 'http://www.exist.ru/price.aspx?pcode=' + user_part.article
    elif user_part.id_number:
        url = 'http://www.exist.ru/price.aspx?pid=' + user_part.id_number
    else:
        raise ValueError

    html = urllib2.urlopen(url)
    soup = BeautifulSoup(html)
    table = soup.find('table', {'class': 'tbl'})

    parts = []

    header = table.findAll('th')
    rows = table.findAll('tr')
    need_rows = []
    for row in rows:
        if not row.attrs:
            break
        if row.attrs[0][0] == 'id':
            if row.attrs[0][1][0] == '_':
                need_rows.append(row)

    firm = need_rows[0].findAll('td')[0].find('div', {'class': 'firmname'}).text
    description = need_rows[0].findAll('td')[0].find('div', {'class': 'descblock'}).text

    for row in need_rows:
        part = {}
        cols = row.findAll('td')

        if len(need_rows) == 1:
            if cols[2].text:
                in_stock = cols[2].text
            else:
                in_stock = 'Склад поставщика'
            if header[3].text == 'Ожидаемый срок, дн.':
                minimal_order = 1
                expected_time = cols[3].text
                price = cols[4].text
            else:
                if cols[3].text:
                    minimal_order = cols[3].text
                else:
                    minimal_order = 1
                expected_time = cols[4].text
                price = cols[5].text

        else:
            if row.attrs[0][1][1] == '0':
                if cols[2].text:
                    in_stock = cols[2].text
                else:
                    in_stock = 'Склад поставщика'

                if header[3].text == 'Ожидаемый срок, дн.':
                    minimal_order = 1
                    expected_time = cols[3].text
                    price = cols[4].text
                else:
                    if cols[3].text:
                        minimal_order = cols[3].text
                    else:
                        minimal_order = 1
                    expected_time = cols[4].text
                    price = cols[5].text
            else:
                if cols[0].text:
                    in_stock = cols[0].text
                else:
                    in_stock = 'Склад поставщика'

                if header[3].text == 'Ожидаемый срок, дн.':
                    minimal_order = 1
                    expected_time = cols[1].text
                    price = cols[2].text
                else:
                    if cols[1].text:
                        minimal_order = cols[1].text
                    else:
                        minimal_order = 1
                    expected_time = cols[2].text
                    price = cols[3].text

        part.update({
            'firm': firm,
            'description': description,
            'in_stock': in_stock,
            'minimal_order': minimal_order,
            'expected_time': expected_time,
            'price': price
        })
        parts.append(part)

        last_part = UserPartInfo.objects.filter(user_part=user_part).latest('created_at')

        if last_part.in_stock == in_stock and last_part.minimal_order == minimal_order \
                and last_part.expected_time == expected_time and last_part.price == price:
            last_part.created_at = datetime.datetime.now()
            last_part.save()
        else:
            UserPartInfo.objects.get_or_create(
                part=user_part, firm=firm, description=description,
                in_stock=in_stock, minimal_order=minimal_order, expected_time=expected_time, price=price
            )
